﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    public int Lives { get { return gameData.Lives; } }

    private GameData gameData;

    public static event Action<int> OnCoinsChanged;

    private void Awake()
    {
        GameController oldController = FindObjectsOfType<GameController>().FirstOrDefault(t => t != this);
        if (oldController != null)
            Destroy(oldController.gameObject);

        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;

        Instance = this;
        ResetGame();
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (OnCoinsChanged != null)
            OnCoinsChanged(gameData.Coins);
    }

    internal void KillPlayer()
    {
        gameData.Lives--;

        if (gameData.Lives > 0)
            RestartCurrentLevel();
        else
            SceneManager.LoadScene("Menu");
    }

    private static void RestartCurrentLevel()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
    }

    private void ResetGame()
    {
        gameData = new GameData();

        if (OnCoinsChanged != null)
            OnCoinsChanged(gameData.Coins);
    }

    public void AddCoin()
    {
        gameData.Coins++;

        if (OnCoinsChanged != null)
            OnCoinsChanged(gameData.Coins);
    }
}
