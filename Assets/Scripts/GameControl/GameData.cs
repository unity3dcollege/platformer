﻿public class GameData
{
    public GameData()
    {
        Lives = 3;
        Coins = 0;
    }

    public int Lives { get; set; }
    public int Coins { get; set; }
}