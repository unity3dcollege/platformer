﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeBox : Activatable
{
    [SerializeField]
    private float totalTime = 1f;
    [SerializeField]
    private Collider2D spikeCollider;
    [SerializeField]
    private Transform startPosition;
    [SerializeField]
    private Transform stopPosition;
    [SerializeField]
    private AnimationCurve movementCurve = new AnimationCurve();

    private bool isMoving;

    [ContextMenu("Activate")]
    public override void Activate()
    {
        isMoving = true;
        StartCoroutine(DoMovement());
    }

    private IEnumerator DoMovement()
    {
        float elapsed = 0f;

        while (elapsed < totalTime)
        {
            float pct = elapsed / totalTime;

            float pointOnCurve = movementCurve.Evaluate(pct);

            Vector3 newPosition = Vector3.Lerp(startPosition.position, stopPosition.position, pointOnCurve);
            spikeCollider.transform.position = newPosition;

            yield return null;
            elapsed += Time.deltaTime;
        }

        isMoving = false;
    }

    public override bool CanActivate()
    {
        return !isMoving;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0f, 1f, 0f, 0.5f);
        Gizmos.DrawCube(startPosition.position, spikeCollider.bounds.size);

        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(startPosition.position, stopPosition.position);

        Gizmos.color = new Color(1f, 0f, 0f, 0.5f);
        Gizmos.DrawCube(stopPosition.position, spikeCollider.bounds.size);
    }
}
