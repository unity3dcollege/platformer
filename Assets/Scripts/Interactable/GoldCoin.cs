﻿using UnityEngine;

public class GoldCoin : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponent<PlayerController>() != null)
        {
            GameController.Instance.AddCoin();
            Destroy(gameObject);
        }
    }
}