﻿using UnityEngine;

public abstract class Activatable : MonoBehaviour
{
    public abstract void Activate();
    public abstract bool CanActivate();
}