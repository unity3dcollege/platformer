﻿using UnityEngine;

public class CoinBox : MonoBehaviour
{
    [SerializeField]
    private int startingCoins = 10;
    [SerializeField]
    private SpriteRenderer coinsAvailableSprite;
    [SerializeField]
    private SpriteRenderer outOfCoinsSprite;
    [SerializeField]
    private ParticleSystem coinParticle;

    private int remainingCoins;

    private void Awake()
    {
        ModifyCoins(startingCoins);
    }

    private void ModifyCoins(int change)
    {
        remainingCoins += change;

        coinsAvailableSprite.enabled = remainingCoins > 0;
        outOfCoinsSprite.enabled = remainingCoins < 1;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (remainingCoins > 0)
        {
            Debug.Log("Got Coin!");
            ModifyCoins(-1);
            coinParticle.Play();

            GameController.Instance.AddCoin();
        }
    }
}