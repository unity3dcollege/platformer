﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableBlock : MonoBehaviour
{
    private SpriteRenderer spriterenderer;
    private ParticleSystem particlesystem;

    private void Awake()
    {
        spriterenderer = GetComponent<SpriteRenderer>();
        particlesystem = GetComponentInChildren<ParticleSystem>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasWithPlayer() && collision.WasOnBottom())
        {
            StartCoroutine(BlowUp());
        }
    }

    private IEnumerator BlowUp()
    {
        spriterenderer.enabled = false;
        particlesystem.Play();
        yield return new WaitForSeconds(particlesystem.main.duration);
        Destroy(gameObject);
    }
}
