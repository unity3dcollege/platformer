﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationTrigger : MonoBehaviour
{
    [SerializeField]
    private Activatable thingToActivate;

    private void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<PlayerController>();

        if (player != null)
        {
            if (thingToActivate.CanActivate())
            {
                thingToActivate.Activate();
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (thingToActivate != null)
        {
            Gizmos.color = new Color(0.5f, 0.5f, 1f, 0.5f);
        }
        else
        {
            Gizmos.color = new Color(0f, 0f, 1f, 0.5f);
        }

        Gizmos.DrawCube(transform.position, GetComponent<Collider2D>().bounds.size);

        if (thingToActivate != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, thingToActivate.transform.position);
        }
    }
}