﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelOnTouch : MonoBehaviour {

    [SerializeField]
    private int levelNumber;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasWithPlayer())
        {
            SceneManager.LoadScene("Level" + levelNumber);
        }
    }
}