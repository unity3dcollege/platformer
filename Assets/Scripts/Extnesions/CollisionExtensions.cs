﻿using UnityEngine;

public static class CollisionExtensions
{
    public static bool WasOnTop(this Collision2D collision)
    {
        return collision.contacts[0].normal.y < -0.8f;
    }

    public static bool WasOnBottom(this Collision2D collision)
    {
        return collision.contacts[0].normal.y > 0.8f;
    }

    public static bool WasOnSide(this Collision2D collision)
    {
        return collision.contacts[0].normal.y < 0.2f && collision.contacts[0].normal.y > -0.2f;
    }

    public static bool WasWithPlayer(this Collision2D collision)
    {
        return collision.collider.GetComponent<PlayerController>() != null;
    }
    
}