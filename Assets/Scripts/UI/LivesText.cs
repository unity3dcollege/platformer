﻿using UnityEngine;

public class LivesText : MonoBehaviour
{
    private void Awake()
    {
        int lives = GameController.Instance.Lives;
        GetComponent<TMPro.TextMeshProUGUI>().text = "x " + lives;
    }
}