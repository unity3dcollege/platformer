﻿using TMPro;
using UnityEngine;

public class CoinsText : MonoBehaviour
{
    private TextMeshProUGUI tmText;

    private void Awake()
    {
        tmText = GetComponent<TMPro.TextMeshProUGUI>();
        GameController.OnCoinsChanged += GameController_OnCoinsChanged;
    }

    private void GameController_OnCoinsChanged(int coins)
    {
        tmText.text = coins.ToString();
    }
}