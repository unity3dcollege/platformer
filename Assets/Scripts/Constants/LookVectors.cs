﻿using UnityEngine;

public static class LookVectors
{
    public static Vector3 Left = Vector3.zero;
    public static Vector3 Right = new Vector3(0, 180, 0);
}