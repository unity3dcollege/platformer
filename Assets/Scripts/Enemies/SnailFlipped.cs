﻿using UnityEngine;

public class SnailFlipped : Snail
{
    [SerializeField]
    private float shellSpeed = 5f;

    private bool isMoving;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasWithPlayer())
        {
            if (velocity.magnitude == 0)
            {
                LaunchShell(collision);
            }
            else
            {
                if (collision.WasOnTop())
                {
                    SetSpeed(0f);
                }
                else
                {
                    GameController.Instance.KillPlayer();
                }
            }
        }
    }

    private void LaunchShell(Collision2D collision)
    {
        var direction = collision.contacts[0].normal.x > 0 ? 1f : -1f;
        SetSpeed(direction * shellSpeed);
    }
}
