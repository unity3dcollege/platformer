﻿using System;
using UnityEngine;

public class MoveHorizontal : MonoBehaviour
{
    [SerializeField]
    private Vector2 velocity;

    private Rigidbody2D rigidbody2d;

    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        rigidbody2d.MovePosition(rigidbody2d.position + velocity * Time.fixedDeltaTime);
    }

    internal void SetSpeed(float shellSpeed)
    {
        velocity = Vector2.right * shellSpeed;
    }

    public void SwitchDirections()
    {
        velocity *= -1f;
    }
}