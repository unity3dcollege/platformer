﻿using UnityEngine;

public class SnailNormal : Snail
{
    [SerializeField]
    private GameObject flippedSnailPrefab;
    [SerializeField]
    private float initialVelocity = 1f;

    private void Start()
    {
        SetSpeed(initialVelocity);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasWithPlayer())
        {
            if (collision.WasOnTop())
            {
                FlipSnail();
            }
            else
            {
                GameController.Instance.KillPlayer();
            }
        }
    }

    private void FlipSnail()
    {
        Destroy(this.gameObject);
        Instantiate(flippedSnailPrefab, transform.position, transform.rotation);
    }
}