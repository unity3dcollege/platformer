﻿using UnityEngine;

public class Snail : MonoBehaviour
{
    [SerializeField]
    protected SpriteRenderer sprite;
    [SerializeField]
    protected Vector2 velocity;

    private Rigidbody2D rigidbody2d;
    private Collider2D collider;

    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
    }

    private void FixedUpdate()
    {
        rigidbody2d.MovePosition(rigidbody2d.position + velocity * Time.fixedDeltaTime);
    }

    private void LateUpdate()
    {
        if (TouchedNonPlayerOnSide())
            SwitchDirections();
    }

    internal void SetSpeed(float shellSpeed)
    {
        velocity = new Vector2(shellSpeed, velocity.y);
    }

    public void SwitchDirections()
    {
        velocity *= -1f;
        sprite.flipX = !sprite.flipX;
    }

    protected bool TouchedNonPlayerOnSide()
    {
        if (velocity.x > 0)
        {
            var rightHit = Physics2D.Raycast(transform.position + new Vector3(collider.bounds.extents.x + 0.1f, 0f, 0f), Vector3.right, 0.1f);
            if (rightHit.collider != null && rightHit.collider.GetComponent<PlayerController>() == null)
                return true;
        }
        else
        {
            var leftHit = Physics2D.Raycast(transform.position - new Vector3(collider.bounds.extents.x + 0.1f, 0f, 0f), Vector3.right * -1f, 0.1f);
            if (leftHit.collider != null && leftHit.collider.GetComponent<PlayerController>() == null)
                return true;
        }

        return false;
    }
}
