﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private Transform target;

    private Vector3 startPosition;

    private void Start()
    {
        startPosition = transform.position;
    }

    private void Update()
    {
        float cameraLeft = Mathf.Max(startPosition.x, target.position.x);
        float cameraHeight = Mathf.Max(startPosition.y, target.position.y);

        transform.position = 
            new Vector3(cameraLeft, cameraHeight, transform.position.z);
    }
}
