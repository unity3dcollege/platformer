﻿using UnityEngine;

public class MoveOnTouch : MonoBehaviour
{
    [SerializeField]
    private Vector3 velocity;
    [SerializeField]
    private Animator animator;
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.WasWithPlayer())
        {
            animator.SetBool("Moving", true);
            collision.collider.transform.SetParent(transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.WasWithPlayer())
        {
            collision.collider.transform.SetParent(null);
        }
    }
}