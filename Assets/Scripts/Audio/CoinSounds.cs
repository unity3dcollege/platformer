﻿using UnityEngine;

public class CoinSounds : MonoBehaviour
{
    [SerializeField]
    private SimpleAudioEvent audioEvent;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        GameController.OnCoinsChanged += GameController_OnCoinsChanged;
    }

    private void OnDestroy()
    {
        GameController.OnCoinsChanged -= GameController_OnCoinsChanged;
    }

    private void GameController_OnCoinsChanged(int coins)
    {
        audioEvent.Play(audioSource);
    }
}