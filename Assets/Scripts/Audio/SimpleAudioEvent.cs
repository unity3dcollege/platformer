﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Audio/Simple")]
public class SimpleAudioEvent : ScriptableObject
{
    [SerializeField]
    private AudioClip[] clips = new AudioClip[0];
    [SerializeField]
    RangedFloat volume = new RangedFloat(1f, 1f);

    public void Play(AudioSource audioSource)
    {
        AudioClip selectClip = ChooseRandomClip();

        audioSource.volume = UnityEngine.Random.Range(volume.minValue, volume.maxValue);
        audioSource.PlayOneShot(selectClip);
    }

    private AudioClip ChooseRandomClip()
    {
        int clipIndex = UnityEngine.Random.Range(0, clips.Length);
        return clips[clipIndex];
    }
}
