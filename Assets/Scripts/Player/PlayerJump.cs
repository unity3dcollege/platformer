﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour
{
    [SerializeField]
    private float jumpForce = 700f;

    private Rigidbody2D rigidbody2d;
    private PlayerGrounding playerGrounding;
    
    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        playerGrounding = GetComponent<PlayerGrounding>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && playerGrounding.IsGrounded())
        {
            Jump();
        }
    }

    private void Jump()
    {
        rigidbody2d.AddForce(Vector2.up * jumpForce);
    }
}
