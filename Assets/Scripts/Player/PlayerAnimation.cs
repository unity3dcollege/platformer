﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    private PlayerController playerController;
    private bool wasMovingLastFrame;

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        bool isMoving = playerController.Motion != 0f;

        if (wasMovingLastFrame != isMoving)
        {
            animator.SetBool("Moving", isMoving);
            wasMovingLastFrame = isMoving;
        }
    }
}