﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 500f;

    private Rigidbody2D rigidbody2d;

    public float Motion { get; private set; }

    private void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Motion = ReadInput();
        Move(Motion);
        Look(Motion);
    }

    private void Look(float motion)
    {
        if (motion != 0f)
        {
            transform.eulerAngles = motion > 0 ? LookVectors.Right : LookVectors.Left;
        }
    }

    private void Move(float motion)
    {
        Vector3 motion2d = Vector3.right * motion * moveSpeed * Time.deltaTime;

        rigidbody2d.velocity = CombineYandYVelocity(rigidbody2d.velocity, motion2d);
    }

    private Vector2 CombineYandYVelocity(Vector2 original, Vector3 motion2d)
    {
        return new Vector2(motion2d.x, original.y);
    }

    private float ReadInput()
    {
        return Input.GetAxis("Horizontal");
    }
}
