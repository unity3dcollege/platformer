﻿using UnityEngine;

public class PlayerGrounding : MonoBehaviour
{
    [SerializeField]
    private Transform feetPositionLeft;
    [SerializeField]
    private Transform feetPositionRight;

    [SerializeField]
    private LayerMask groundMask;

    private void Update()
    {
        IsGrounded();
    }

    public bool IsGrounded()
    {
        return IsFeetPositionGrounded(feetPositionRight) || IsFeetPositionGrounded(feetPositionLeft);
    }

    private bool IsFeetPositionGrounded(Transform feetPosition)
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(feetPosition.position, Vector2.down, 0.2f, (int)groundMask);

        if (hits.Length > 0)
        {
            Debug.DrawRay(feetPosition.position, Vector3.down, Color.green);
            return true;
        }
        else
        {
            Debug.DrawRay(feetPosition.position, Vector3.down * 0.2f, Color.red);
            return false;
        }
    }
}
