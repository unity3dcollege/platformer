﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSection : MonoBehaviour
{
    [SerializeField]
    private Color color = Color.yellow;

    private Bounds bounds;

    private void OnDrawGizmos()
    {
        bounds = new Bounds(transform.position, Vector3.zero);

        BoxCollider2D[] boxColliders = GetComponentsInChildren<BoxCollider2D>();
        foreach (BoxCollider2D collider in boxColliders)
        {
            bounds.Encapsulate(collider.bounds);
        }

        Gizmos.color = color;
        Gizmos.DrawWireCube(bounds.center, bounds.extents * 2f);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = color;
        
        BoxCollider2D[] boxColliders = GetComponentsInChildren<BoxCollider2D>();

        foreach (BoxCollider2D collider in boxColliders)
        {
            Gizmos.DrawCube(collider.bounds.center, collider.size);

            bounds.Encapsulate(collider.bounds);
        }

        Gizmos.DrawWireCube(bounds.center, bounds.extents * 2f);
    }

}
